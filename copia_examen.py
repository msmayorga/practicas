#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import csv

#Reads a file and adds a header. The names are a short description of the content of the column
def read(filename):
    df = pd.read_csv(filename, sep = ' ', names =["ages", "sex", "chest_pain_type", "resting_blood_pleasure", "serum_cholestoral", "fasting_blood_sugar", "resting_electrocardiographic_results", "maximum_heart_rate", "excercise_induced", "oldpeak", "slope_peak_excercise", "major_vessels", "thal", "heart_disease", "blood_pressure_deviation"])
    return (df)
#Makes the calculus of the blood preassure deviation
def calculate():
    d = read("heart.dat")
    x = d["resting_blood_pleasure"]
    mu = np.mean(x)
    sigma = np.std(x)
    bpd = (x-mu)/sigma
    #append the value of bpd in the DataFrame
    d["blood_pressure_deviation"].fillna(bpd, inplace = True)
    d.to_csv('test')
    return d

if __name__ == "__main__":
    print(calculate())

