#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import csv

#Reads a file
def read(filename, separator=','):
    df = pd.read_csv(filename, sep=separator)
    return df
#lowers the text
def changes_text():
    d = read('SalesJan2009.csv')
    lowered = d.applymap(lambda x: str(x).lower())
    lowered.to_csv('test_1')
    print(lowered)
#capitalise the headers
#def header():
 #   d = read(y)
    #capitalised = d.apply(x str(x).upper() for x in d)
    #capitalised.to_csv('test_2')
  #  print(capitalised)

if __name__ == "__main__":
    changes_text()